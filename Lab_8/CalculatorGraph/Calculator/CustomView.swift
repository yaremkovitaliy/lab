 
import UIKit

class CustomView: UIView {
    
    var minimum:CGFloat! {
        set {
            if newValue != nil{
                start = newValue * bounds.maxX / 100
            }
            else {
                start = bounds.minX
            }
            setNeedsDisplay()
        }
        get {
            return bounds.minX
        }
    }
    var maximum:CGFloat! {
        set {
            if newValue != nil{
                end = newValue * bounds.maxX / 100
            }
            else
            {
                end = bounds.maxX
            }
            setNeedsDisplay()
        }
        get {
            return  bounds.maxX
        }
    }
    var startPoint:CGPoint?
    var origin:CGPoint {
        get {
            return startPoint ?? CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        }
        set {
            startPoint = newValue
        }
    }
    var a:Double = 0 {
        didSet{
            if a != oldValue{
                setNeedsDisplay()
            }
        }
    }
    var b:Double = 0 {
        didSet{
            if b != oldValue{
                setNeedsDisplay()
            }
        }
    }
    var c:Double = 0 {
        didSet{
            if c != oldValue{
                setNeedsDisplay()
            }
        }
    }
    var start:CGFloat?
    var end:CGFloat?
    var step:Double = 1
        {
        didSet{
            if step != oldValue{
                setNeedsDisplay()
            }
        }
    }
    //    var scale:CGFloat {
    //        if start == nil || end == nil {
    //            return 50
    //        }
    //        else if start != bounds.minX || end != bounds.maxX {
    //            return (50-(abs(start!-end!)/9))+50
    //        }
    //        else{
    //            return 50
    //        }
    //    }
    var myCustomY:Double {
        get {
            switch myYFunction {
            case "F1":
                return a*x!+b
            case "F2":
                return a*cos(b*x!+c)
            case "F3":
                return a*sin(b*x!+c)
            case "F4":
                return pow((a*x!+b),c)
            default:
                return 0
            }
        }
    }
    var x,y:Double?
    func buildAxes(){
        let axes = UIBezierPath()
        axes.move(to:CGPoint(x: bounds.midX, y: bounds.minY))
        axes.addLine(to: CGPoint(x:bounds.midX ,y: bounds.maxY))
        axes.move(to:CGPoint(x:bounds.minX,y:bounds.midY))
        axes.addLine(to:CGPoint(x:bounds.maxX,y:bounds.midY))
        axes.lineWidth = 3.0
        axes.stroke()
    }
    func drawGraph(){
        if a != 0 && b != 0 && c != 0 {
            let graph = UIBezierPath()
            graph.lineWidth = 3.5
            let scale:CGFloat = 50
            UIColor.yellow.setStroke()
            var xGraph , yGraph:CGFloat
            var isFirstPoint = true
            
            for i in 0...Int(bounds.size.width * contentScaleFactor) {
                xGraph = CGFloat(i*Int(step)) / contentScaleFactor
                x = Double((xGraph - origin.x)/scale)
                y = myCustomY
                yGraph = origin.y - CGFloat(y!) * scale
                if isFirstPoint {
                    if start != nil {
                        if xGraph > start!{
                            graph.move(to:CGPoint(x:xGraph,y:yGraph))
                            isFirstPoint = false
                        }
                    }
                    else {
                        graph.move(to:CGPoint(x:xGraph,y:yGraph))
                        isFirstPoint = false
                    }
                }
                else {
                    if end != nil  {
                        if end! > xGraph{
                            graph.addLine(to: CGPoint(x: xGraph, y: yGraph))
                        }
                    }
                    else {
                        graph.addLine(to: CGPoint(x: xGraph, y: yGraph))
                    }
                }
            }
            graph.stroke()
        }
    }
    
    override func draw(_ rect: CGRect) {
        buildAxes()
        drawGraph()
    }
    
}
