 
import Foundation

func multiply (op1: Double, op2: Double) -> Double {
    return op1 * op2
}
func changeSign (op1: Double) -> Double {
    return -op1
}
func dividing (op1:Double , op2:Double) ->Double {
    return op1 / op2
}
func addition (op1:Double , op2:Double) ->Double {
    return op1 + op2
}
func subtraction (op1:Double , op2:Double) ->Double {
    return op1 - op2
}
func doPersents (op1:Double)->Double {
    return op1*0.01
}
func pow (op1:Double)->Double {
    return op1*op1
}

struct CalculatorModel {
    
    private var accumulator:Double?
    private var pendingBinaryOperation: PendingBinaryOperation?
    var toShow = ""
    private var resultPending=false
    var result: Double? {
        get {
            if accumulator != nil {
                return accumulator!
            }
            return 0.0
        }
    }
    var description:String {
        get {
            return toShow
        }
        set{
            if resultPending{
                toShow += newValue
            }
            else {
                toShow = newValue + "(" + toShow + ")"
            }
        }
    }
    private enum Operation {
        case constant (Double)
        case unaryOperation ((Double) -> Double)
        case binaryOperation ((Double, Double) -> Double)
        case equals
    }
    
    private var operations : Dictionary <String,Operation> =
        [
            "π": Operation.constant (Double.pi),
            "e": Operation.constant (M_E),
            "√": Operation.unaryOperation (sqrt),
            "sin": Operation.unaryOperation (sin),
            "cos": Operation.unaryOperation (cos),
            "tan": Operation.unaryOperation(tan),
            "±": Operation.unaryOperation (changeSign),
            "%": Operation.unaryOperation (doPersents),
            "x^2":Operation.unaryOperation (pow),
            "×": Operation.binaryOperation (multiply),
            "÷": Operation.binaryOperation (dividing),
            "+": Operation.binaryOperation (addition),
            "-": Operation.binaryOperation (subtraction),
            "=": Operation.equals,
            ]
    
    mutating func performOperation(_ symbol: String) {
        if let operation = operations[symbol]{
            switch operation {
            case .constant(let value):
                if resultPending {
                    description = String(format:"%g",value)
                    accumulator = value
                }
                else {
                    toShow =  " "
                    toShow += String(format:"%g",value)
                    accumulator = value
                    resultPending = true
                }
            case .unaryOperation (let function):
                if accumulator != nil {
                    resultPending = false
                    description = symbol
                    accumulator = function (accumulator!)
                    resultPending = true
                }
            case .binaryOperation (let function):
                if accumulator != nil {
                    if resultPending {
                        description = symbol
                        performPendingBinaryOperation()
                        pendingBinaryOperation = PendingBinaryOperation(function: function, firstOperand: accumulator!)
                    }
                    else {
                        toShow =  " "
                        toShow += symbol
                        performPendingBinaryOperation()
                        pendingBinaryOperation = PendingBinaryOperation(function: function, firstOperand: accumulator!)
                        resultPending = true
                    }
                }
            case .equals:
                if accumulator != nil {
                    performPendingBinaryOperation()
                    resultPending = false
                }
            }
        }
    }
    
    private mutating func performPendingBinaryOperation() {
        if pendingBinaryOperation != nil{
            accumulator! =  pendingBinaryOperation!.function(pendingBinaryOperation!.firstOperand, accumulator!)
            pendingBinaryOperation = nil
        }
    }
    
    private struct PendingBinaryOperation {
        let function: (Double,Double) -> Double
        let firstOperand: Double
    }
    mutating func clear() {
        toShow =  ""
        accumulator = nil
        pendingBinaryOperation=nil
    }
    mutating func setOperand (operand: Double){
        if !resultPending{
            toShow = " "
            accumulator = operand
            resultPending = true
            description = String(format:"%g",operand)
        }
        else
        {
            accumulator = operand
            resultPending = true
            description = String(format:"%g",operand)
        }
    }
}
