 
import UIKit

var myYFunction = ""

class GraphViewController: UIViewController {
    
    @IBOutlet weak var myCustomView: CustomView!
    @IBOutlet weak var aOutlet: UITextField!
    @IBOutlet weak var bOutlet: UITextField!
    @IBOutlet weak var cOutlet: UITextField!
    @IBOutlet weak var startOtlet: UITextField!
    @IBOutlet weak var endOutlet: UITextField!
    @IBOutlet weak var stepOutlet: UITextField!
    @IBOutlet weak var typeOfGraph: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        switch myYFunction {
        case "F1" :
            typeOfGraph.text = "ax+b"
        case "F2":
            typeOfGraph.text = "a*cos(bx+c)"
        case "F3":
            typeOfGraph.text = "a*sin(bx+c)"
        case "F4":
            typeOfGraph.text = "(ax+b)^c"
        default:
            break
        }
    }
    func createAllert(message:String){
        let allert = UIAlertController(title: "Warning", message: message, preferredStyle: UIAlertControllerStyle.alert)
        allert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            allert.dismiss(animated: true, completion: nil)
        }))
        self.present(allert, animated: true, completion: nil)
    }

    @IBAction func buildButton(_ sender: UIButton) {
        if Double(aOutlet.text!) != nil {
            myCustomView.a = Double(aOutlet.text!)!
        }
        else {
            createAllert(message: "Type correct number in (a)")
        }
        
        if Double(bOutlet.text!) != nil {
            myCustomView.b = Double(bOutlet.text!)!
        }
        else {
            createAllert(message: "Type correct number in (b)")
        }
        
        if Double(cOutlet.text!) != nil {
            myCustomView.c = Double(cOutlet.text!)!
        }
        else {
            createAllert(message: "Type correct number in (c)")
        }
        
        if Double(startOtlet.text!) != nil {
            if Double(startOtlet.text!)! >= 0 && Double(startOtlet.text!)! <= 100{
                if Double(endOutlet.text!) != nil{
                    if Double(startOtlet.text!)! < Double(endOutlet.text!)!{
                        myCustomView.minimum = CGFloat(Double(startOtlet.text!)!)
                    }
                    else {
                        createAllert(message: "Start < End !!!")
                    }
                }
            }
            else {
                createAllert(message: "Start [ 0;100 ]")
            }
        }
        else {
            createAllert(message: "Start is empty or incorrect")
        }
        
        if Double(endOutlet.text!) != nil {
            if Double(endOutlet.text!)! >= 0 && Double(endOutlet.text!)! <= 100{
                if Double(startOtlet.text!) != nil {
                    if Double(startOtlet.text!)! < Double(endOutlet.text!)!{
                        myCustomView.maximum = CGFloat(Double(endOutlet.text!)!)
                    }
                    else {
                        createAllert(message: "Start < End !!!")
                    }
                }
            }
            else {
                createAllert(message: "End [ 0;100 ]")
            }
        }
        else {
            createAllert(message: "End is empty or incorrect")
        }
        if Double(stepOutlet.text!) != nil {
            if Double(stepOutlet.text!)! >= 1{
                myCustomView.step = Double(stepOutlet.text!)!
            }
            else {
                createAllert(message: "Step > 1 !!!")
            }
        }
        else {
            createAllert(message: "Step is empty or incorrect")
        }    }
    
}

