 
import UIKit



class CalculatorViewController: UIViewController {
    
    @IBOutlet weak var inputDigitsField: UILabel!
    @IBOutlet weak var clearButtonsStance: UIButton!
    @IBOutlet weak var myDescription: UILabel!
    private var model = CalculatorModel()
    private var isTyping=false
    private var dispayValue:Double {
        get{
            return Double(inputDigitsField.text!)!
        }
        set{
            inputDigitsField.text = String(format: "%g",newValue)
        }
    }
    
    @IBAction private func digits(_ sender: UIButton) {
        if isTyping {
            if sender.currentTitle == "."{
                if inputDigitsField.text?.range(of: ".") == nil{
                    inputDigitsField.text = inputDigitsField.text! + "."
                }
            }
            else{
                inputDigitsField.text = inputDigitsField.text! + sender.currentTitle!
            }
        }
        else {
            if sender.currentTitle == "." {
                if !(inputDigitsField.text?.contains("."))!
                {
                    inputDigitsField.text = inputDigitsField.text! + sender.currentTitle!
                    isTyping=true
                }
            }
            if sender.currentTitle != "0" && sender.currentTitle != "."  {
                inputDigitsField.text = sender.currentTitle!
                isTyping=true
            }
        }
    }
    
    @IBAction func ClearInputDigitField(_ sender: UIButton) {
        model.clear()
        myDescription.text = " "
        dispayValue = 0
        isTyping = false
    }
    
    
    @IBAction private func preformOperation(_ sender: UIButton) {
        if isTyping {
            model.setOperand(operand:dispayValue)
            isTyping=false
        }
        if let mathSymbol = sender.currentTitle{
            model.performOperation(mathSymbol)
            if mathSymbol == "="{
                if myDescription.text != " " {
                    myDescription.text = model.description + "="
                    dispayValue = model.result!
                }
            }
            else {
                dispayValue = model.result!
                myDescription.text = model.description
            }
        }
    }
    @IBAction func getYFunction(_ sender: UIButton) {
        switch sender.currentTitle! {
        case "F1" :
            myYFunction = "F1"
        case "F2":
            myYFunction = "F2"
        case "F3":
            myYFunction = "F3"
        case "F4":
            myYFunction = "F4"
        default:
            break
        }
    }

}
