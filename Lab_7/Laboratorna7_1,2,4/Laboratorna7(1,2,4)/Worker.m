 

#import <Foundation/Foundation.h>
#import "Worker.h"

@implementation Worker:NSObject

-(id)initWith:(NSString*)name surname:(NSString*)surname sallary:(int)sallary experience:(int)experience dateOfAccepting:(NSString*)dateOfAccepting departament:(NSString*)departament{
    NSNumber *currentSallary = [[NSNumber alloc]initWithInt:sallary];
    NSNumber *currentExperience = [[NSNumber alloc]initWithInt:experience];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    NSDate *dateString = [dateFormatter dateFromString:dateOfAccepting];
    _name = name;
    _surname = surname;
    _sallary = currentSallary;
    _experience = currentExperience;
    _dateOfAccepting = dateString;
    _departament = departament;
    return self;
}
@end
