 
#import <Foundation/Foundation.h>

#import "Worker.h"
#import "Work.h"
#import "Accounting.h"


@implementation Accounting : NSObject

-(void)setSallaryForWorker:(Worker*)worker sallary:(int)sallary{
    Work *sharedInstance = [Work sharedInstance];
    if ([sharedInstance.workersArray containsObject:worker]){
        NSNumber *currentSallary = [[NSNumber alloc]initWithInt:sallary];
        worker.sallary = currentSallary;
    }
}
-(void)setSallaryByExp:(Worker*)worker{
    Work *sharedInstance = [Work sharedInstance];
    if ([sharedInstance.workersArray containsObject:worker]){
        NSNumber* constant = [[NSNumber alloc]initWithInt:300];
        worker.sallary = @([constant intValue]+[worker.sallary intValue]);
    }
}

-(void)setSallaryForTime:(Worker*)worker timeInHours:(int)time{
    Work *sharedInstance = [Work sharedInstance];
    if ([sharedInstance.workersArray containsObject:worker]){
        NSNumber* currentTime = [[NSNumber alloc]initWithInt:time];
        NSNumber* constant = [[NSNumber alloc]initWithInt:40];
        worker.sallary = @([constant intValue]*[currentTime intValue]);
    }
}
-(void)setSallaryFromTimeToTime:(Worker*)worker endTime:(NSString*)endTime{
    Work *sharedInstance = [Work sharedInstance];
    if ([sharedInstance.workersArray containsObject:worker]){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSDate *date = [dateFormatter dateFromString:endTime];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *componentOne = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
        NSDateComponents *componentTwo = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:worker.dateOfAccepting];
        NSInteger endHour = [componentOne hour];
        NSInteger startHour = [componentTwo hour];
        NSInteger resultedSallary = endHour - startHour;
        NSNumber *currentSallary = [[NSNumber alloc]initWithInteger:resultedSallary];
        worker.sallary = currentSallary;
    }
    
}
@end

