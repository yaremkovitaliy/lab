 
#import <Foundation/Foundation.h>
#import "Worker.h"

@interface Worker : NSObject
{ 
}
@property NSString *name;
@property NSString *surname;
@property NSNumber *sallary;
@property NSNumber *experience;
@property NSDate *dateOfAccepting;
@property NSString *departament;
-(id)initWith:(NSString*)name surname:(NSString*)surname sallary:(int)sallary experience:(int)experience dateOfAccepting:(NSString*)dateOfAccepting departament:(NSString*)departament;
@end
