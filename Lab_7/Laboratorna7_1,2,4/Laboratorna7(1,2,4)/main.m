 
#import <Foundation/Foundation.h>
#import "Worker.h"
#import "Work.h"
#import "Accounting.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Worker *test1 = [[Worker alloc]initWith:@"Vlad" surname:@"Shmatok" sallary:0 experience:1 dateOfAccepting:@
                         "10/10/2017" departament:@"IOS"];
        Work *sharedInstance = [Work sharedInstance];
        Accounting *test = [[Accounting alloc]init];
        sharedInstance.delegate = test;
        @try{
            [sharedInstance acceptOnWork:test1];
        }
        @catch(NSException *exception){
            NSLog(@"Problem!!! Caught exception: %@", [exception name]);
        }
    }
    return 0;
}
