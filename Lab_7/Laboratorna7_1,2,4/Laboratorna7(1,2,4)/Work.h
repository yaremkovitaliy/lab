 
#import <Foundation/Foundation.h>
#import "Worker.h"
#import "Work.h"
#import "Accounting.h"
@interface Work : NSObject
{
}
@property NSMutableArray *workersArray;
@property (nonatomic, weak) id <ProtocolDelegate> delegate;
+ (instancetype)sharedInstance;
-(void)acceptOnWork:(Worker*)worker;
-(void)fire:(Worker*)worker;
-(void)find:(Worker*)worker;
-(void)moveToOtherDepartamet:(Worker*)worker departament:(NSString*)newDepartament;
@end
