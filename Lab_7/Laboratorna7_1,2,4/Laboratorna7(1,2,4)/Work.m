 
#import <Foundation/Foundation.h>
#import "Worker.h"
#import "Work.h"

@implementation Work:NSObject

+ (instancetype)sharedInstance {
    static Work *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Work alloc] init];
        sharedInstance.workersArray = [[NSMutableArray alloc]init];
    });
    return sharedInstance;
}
-(void)acceptOnWork:(Worker*)worker{
    if (![_workersArray containsObject:worker]){
        NSException *badExpException = [NSException
                                        exceptionWithName:@"BadExpException"
                                        reason:@"Experience < 0"
                                        userInfo:nil];
        if ([worker.experience intValue] <= 0){
            @throw badExpException;
        }
        [_workersArray addObject:worker];
        [self.delegate setSallaryForWorker:worker sallary:300];
    }
}
-(void)fire:(Worker*)worker{
    if ([_workersArray containsObject:worker]){
        [_workersArray removeObject:worker];
    }
}
-(void)find:(Worker*)worker{
    if ([_workersArray containsObject:worker]){
        NSLog(@"This person is working here");
    }
}
-(void)moveToOtherDepartamet:(Worker *)worker departament:(NSString*)newDepartament{
    if ([_workersArray containsObject:worker]){
        worker.departament = newDepartament;
    }
}
@end
