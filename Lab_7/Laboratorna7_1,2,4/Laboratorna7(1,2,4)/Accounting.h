 
#import <Foundation/Foundation.h>
#import "Worker.h"
#import "Work.h"

@protocol ProtocolDelegate<NSObject>
@optional
-(void)setSallaryForWorker:(Worker*)worker sallary:(int)sallary;
@end

@interface Accounting:NSObject <ProtocolDelegate>
-(void)setSallaryForWorker:(Worker*)worker sallary:(int)sallary;
-(void)setSallaryByExp:(Worker*)worker;
-(void)setSallaryForTime:(Worker*)worker timeInHours:(int)time;
-(void)setSallaryFromTimeToTime:(Worker*)worker endTime:(NSString*)endTime;
@end
