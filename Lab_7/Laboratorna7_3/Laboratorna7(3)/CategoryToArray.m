 

#import <Foundation/Foundation.h>
#import "CategoryToArray.h"

@implementation NSMutableArray (Util)
- (NSNumber *)findMin :(NSMutableArray*)array{
    NSNumber *min=[array valueForKeyPath:@"@min.intValue"];
    return min;
}
- (NSNumber *)findMax:(NSMutableArray*)array{
    NSNumber *max=[array valueForKeyPath:@"@max.intValue"];
    return max;
}
- (NSNumber *)findAvg:(NSMutableArray*)array{
    NSNumber *avg=[array valueForKeyPath:@"@avg.intValue"];
    return avg;
}

@end
