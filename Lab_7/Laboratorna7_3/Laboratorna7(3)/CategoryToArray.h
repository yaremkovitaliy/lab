 
#import <Foundation/Foundation.h>

@interface NSMutableArray (Util)
- (NSNumber *)findMin:(NSMutableArray*)array;
- (NSNumber *)findMax:(NSMutableArray*)array;
- (NSNumber *)findAvg:(NSMutableArray*)array;
@end
