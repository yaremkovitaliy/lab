//: Playground - noun: a place where people can play

import UIKit
protocol SalarySetterDelegate:class{
    func setSalary(salary:Double) throws -> Double
    func setSalaryPerHour() throws -> Double
}
enum Errors:Error {
    case invalidDates
    case invalidSalary
    case badWorkersName
    case badExp
}

class Worker {
    var name:String
    var surname:String
    var salary:Double
    var departament:String
    var dateOfBirth:Date
    var dateOfAccepting:Date
    var experiense:Double
    var workPerHour:Bool
    var dateOfFireing:Date
    init(name:String,surname:String,salary:Double,departament:String,dateOfBirth:String,dateOfAccepting:String,dateOfFireing:String,experiense:Double,workPerHour:Bool) {
        let formater = DateFormatter()
        formater.dateFormat = "dd:MM:y"
        self.name = name
        self.salary = salary
        self.surname = surname
        self.dateOfBirth = formater.date(from: dateOfBirth)!
        self.dateOfAccepting = formater.date(from: dateOfAccepting)!
        self.departament = departament
        self.dateOfFireing = formater.date(from: dateOfFireing)!
        self.experiense = experiense
        self.workPerHour = workPerHour
    }
}

class Work {
    var workers = [Worker]()
    weak var delegate:SalarySetterDelegate?
    func hireForWork(worker:Worker) throws {
        guard worker.dateOfBirth.compare(worker.dateOfAccepting) == ComparisonResult.orderedAscending else { throw Errors.invalidDates}
        guard  worker.salary >= 0 else {throw Errors.invalidSalary}
        workers.append(worker)
        try worker.salary = (delegate?.setSalary(salary:123.12))!

    }
    func fireFromWork(name:String) throws {
        let workerIndex = workers.index { (item) -> Bool in
            item.name == name
        }
        if let index = workerIndex {
            try workers[index].salary = (delegate?.setSalary(salary:0.0))!
            workers.remove(at: index)
        }
        else {
            throw Errors.badWorkersName
        }
    }
    func transfer(name:String,departament:String) throws {
        let workerIndex = workers.index { (item) -> Bool in
            item.name == name
        }
        if let index = workerIndex {
            workers[index].departament = departament
            try workers[index].salary = (delegate?.setSalaryPerHour())!
        }
        else {
            throw Errors.badWorkersName
        }
    }
}
class Accounting:SalarySetterDelegate {
    let work = Work()
    internal func setSalaryPerHour() throws ->Double{
                return 30*40
    }
    internal func setSalary(salary: Double) throws -> Double {
        if salary >= 0{
            return salary
        }
        else {
            throw Errors.invalidSalary
        }
    }
}


let myWork = Work()
var ac = Accounting()
var first = Worker(name: "Vlad", surname: "Shmatok", salary: 0, departament: "IOS", dateOfBirth: "01:01:1997", dateOfAccepting: "10:07:2016", dateOfFireing: "10:07:2017", experiense: 2, workPerHour: true)
myWork.delegate = ac
do{
try myWork.hireForWork(worker:first)
first.salary
try myWork.transfer(name: "Vlad", departament: "Android")
first.salary
try myWork.fireFromWork(name: "Vlad")
first.salary
myWork.workers
}
catch Errors.invalidDates{
    print("Enter correct dates")
}
catch Errors.invalidSalary {
    print("Enter correct salary")
}
catch Errors.badWorkersName {
    print("No such worker")
}
catch Errors.badExp {
    print("Enter experiense is u want to use this method")
}
