//: Playground - noun: a place where people can play

import UIKit
import Foundation

// Zavdannya 2

extension Work {
    func changeSalaryByExp(name:String) throws {
        let workerIndex = workers.index { (item) -> Bool in
            item.name == name
        }
        if let index = workerIndex {
            if workers[index].experiense != 0{
                workers[index].salary = 1300*workers[index].experiense
            }
            else {
                throw Errors.badExp
            }
        }
        else {
            throw Errors.badWorkersName
        }
    }
    
    func changeSalaryPerHour(name:String) throws {
        let workerIndex = workers.index { (item) -> Bool in
            item.name == name
        }
        if let index = workerIndex {
            if workers[index].workPerHour{
                workers[index].salary = 30*40
            }
            else {
                throw Errors.invalidSalary
            }
        }
        else {
            throw Errors.badWorkersName
        }
    }
    
    func changeSlaryForTime(name:String) throws {
        let formater = DateFormatter()
        formater.dateFormat = "dd:MM:y"
        let workerIndex = workers.index { (item) -> Bool in
            item.name == name
        }
        if let index = workerIndex {
            let end = workers[index].dateOfFireing
            let start = workers[index].dateOfAccepting
            let endInMs = end.timeIntervalSince1970/3600
            let startInMs = start.timeIntervalSince1970/3600
            let time = Double(endInMs) - Double(startInMs)
            if workers[index].workPerHour{
                workers[index].salary = time * 30.0
            }
            else {
                throw Errors.invalidSalary
            }
        }
        else {
            throw Errors.badWorkersName
        }
        
    }
}

// Zavdannya 1

enum Errors:Error {
    case invalidDates
    case invalidSalary
    case badWorkersName
    case badExp
}
class Worker {
    var name:String
    var surname:String
    var salary:Double
    var departament:String
    var dateOfBirth:Date
    var dateOfAccepting:Date
    var experiense:Double
    var workPerHour:Bool
    var dateOfFireing:Date
    init(name:String,surname:String,salary:Double,departament:String,dateOfBirth:String,dateOfAccepting:String,dateOfFireing:String,experiense:Double,workPerHour:Bool) {
        let formater = DateFormatter()
        formater.dateFormat = "dd:MM:y"
        self.name = name
        self.salary = salary
        self.surname = surname
        self.dateOfBirth = formater.date(from: dateOfBirth)!
        self.dateOfAccepting = formater.date(from: dateOfAccepting)!
        self.departament = departament
        self.dateOfFireing = formater.date(from: dateOfFireing)!
        self.experiense = experiense
        self.workPerHour = workPerHour
    }
}

class Work {
    var workers = [Worker]()
    func hireForWork(worker:Worker) throws {
        guard worker.dateOfBirth.compare(worker.dateOfAccepting) == ComparisonResult.orderedAscending else { throw Errors.invalidDates}
        guard  worker.salary > 0 else {throw Errors.invalidSalary}
        workers.append(worker)
    }
    func fireFromWork(name:String) throws {
        let workerIndex = workers.index { (item) -> Bool in
            item.name == name
        }
        if let index = workerIndex {
            workers.remove(at: index)
        }
        else {
            throw Errors.badWorkersName
        }
    }
    func changeSalaty(name:String,salary:Double) throws {
        let workerIndex = workers.index { (item) -> Bool in
            item.name == name
        }
        if let index = workerIndex {
            workers[index].salary = salary
        }
        else {
            throw Errors.invalidSalary
        }
    }
    func findWorker(name:String) throws {
        let workerIndex = workers.index { (item) -> Bool in
            item.name == name
        }
        if let index = workerIndex {
            print("Woker found: \(workers[index].name) \(workers[index].surname) \(workers[index].departament)")
        }
        else {
            throw Errors.badWorkersName
        }
    }
    
}
var work = Work()
do{
    try work.hireForWork(worker:Worker(name: "Vlad", surname: "Shmatok", salary: 300, departament: "IOS", dateOfBirth: "01:01:1997", dateOfAccepting: "10:03:2016", dateOfFireing: "13:03:2016", experiense: 2, workPerHour: true))
    try work.changeSalaty(name: "Vlad", salary: 123)
    //try work.findWorker(name: "Vlad")
    //try work.changeSalaryByExp(name:"Vlad")
    //try work.changeSalaryPerHour(name: "Vlad")
    try work.changeSlaryForTime(name: "Vlad")
    //try work.fireFromWork(name: "Vlad")
    
}
catch Errors.invalidDates{
    print("Enter correct dates")
}
catch Errors.invalidSalary {
    print("Enter correct salary")
}
catch Errors.badWorkersName {
    print("No such worker")
}
catch Errors.badExp {
    print("Enter experiense is u want to use this method")
}
work.workers
work.workers[0].dateOfFireing

//Zavdannya 3

extension Array where Element:FloatingPoint{
    
    func maximum() -> Element {
            var currentMax = self[0]
            for items in self[0...self.count-1] {
                if items > currentMax {
                    currentMax = items
                }
            }
            return currentMax
        }
    func minumum() -> Element {
        var currentMax = self[0]
        for items in self[0...self.count-1] {
            if items < currentMax {
                currentMax = items
            }
        }
        return currentMax
    }
    func average() -> Element {
        var currentSum:Element = 0
        for items in self[0...self.count-1] {
            currentSum += items
        }
        return currentSum / Element(self.count)
    }

}
var ar = [2.2,3.3,-2.3]
var a = ar.maximum()
var b = ar.minumum()
var c = ar.average()
print(a)
print(b)
print(c)


