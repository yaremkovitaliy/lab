 
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var taskOneButton: UIButton!
    @IBOutlet weak var taskTwoButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        taskOneButton.layer.cornerRadius = 8
        taskTwoButton.layer.cornerRadius = 8
    }
    @IBAction func segueToTask(_ sender: UIButton) {
        if sender.titleLabel?.text == "Task 2"{
            performSegue(withIdentifier: "task2", sender: nil)
        }
        else {
           performSegue(withIdentifier: "task3", sender: nil) 
        }
    }
    
    
}

