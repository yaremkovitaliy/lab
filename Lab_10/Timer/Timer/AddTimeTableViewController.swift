 

import UIKit
protocol TalbleViewDelegate{
    func addCity(selectedCity:String)
}

class AddTimeTableViewController: UITableViewController {
    let cities = ["Kyiv","London"]
    var delegate:TalbleViewDelegate? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.black
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return cities.count
    }

    @IBAction func cancelAddind(_ sender: UIBarButtonItem) {
        _=navigationController?.popViewController(animated: true)
    }
  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = cities[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        cell.backgroundColor = UIColor.darkGray
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let city = cities[row]
        delegate?.addCity(selectedCity: city)
        _ = self.navigationController?.popViewController(animated: true)
    }
    }
