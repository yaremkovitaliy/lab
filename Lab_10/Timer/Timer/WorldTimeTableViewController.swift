 
import UIKit

class WorldTimeTableViewController: UITableViewController,TalbleViewDelegate{
    private var timer = Timer()
    var selectedCity = [String]()
    var currentDate = " "
    func addCity(selectedCity:String) {
        self.selectedCity.append(selectedCity)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "adding"
        {
            let secondVC = segue.destination as! AddTimeTableViewController
            secondVC.delegate = self
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()    }
    func update() {
        if tableView.isEditing == false{
            tableView.reloadData()
        }
    }
    func setTime (city:String){
        let date = NSDate()
        let dateFormat = DateFormatter()
        if city == "London"
        {
            dateFormat.timeZone = NSTimeZone(name: "GMT+0100") as! TimeZone
        }
        dateFormat.dateFormat = "HH:mm"
        currentDate = dateFormat.string(from: date as Date)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isEditing = false
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        self.tableView.backgroundColor = UIColor.black
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.orange
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
}
override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}

override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return selectedCity.count
}
override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cells", for: indexPath) as! CustomTableViewCell
    cell.cityLabel.text = selectedCity[indexPath.row]
    setTime(city: cell.cityLabel.text!)
    cell.timeLabel.text = currentDate
    cell.timeLabel.textColor = UIColor.white
    cell.backgroundColor = UIColor.darkGray
    switch  cell.cityLabel.text! {
    case "Kyiv":
        cell.descriptionLabel.text = "Today , +2"
    default:
        cell.descriptionLabel.text = "Today , -2"
    }
    return cell
}
override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
    let movedObject = self.selectedCity[sourceIndexPath.row]
    selectedCity.remove(at: sourceIndexPath.row)
    selectedCity.insert(movedObject,at:destinationIndexPath.row)
}
override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
}

override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
        selectedCity.remove(at: indexPath.row)
        self.tableView.reloadData()
        
    }
}
}
