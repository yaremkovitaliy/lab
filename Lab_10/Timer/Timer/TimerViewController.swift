 

import UIKit
import QuartzCore
class TimerViewController:UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    private var timer = Timer()
    var selectedHours = 0
    var selectedMinutes = 0
    var seconds = 60
    @IBOutlet weak var myTimePicker: UIPickerView!
    @IBOutlet weak var display: UILabel!

    var hoursData = [Int]()
    var minutesData = [Int]()
    var dataPicker = [[Int]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        display.isHidden = true
        hoursData.append(contentsOf: 0...23)
        minutesData.append(contentsOf: 0...59)
        dataPicker = [hoursData,minutesData]
        myTimePicker.backgroundColor = UIColor.black
        
        let columnHourView = UILabel(frame: CGRect(x: myTimePicker.bounds.width/2-(myTimePicker.bounds.width/2)/3 , y: myTimePicker.bounds.height/2 - 15, width: myTimePicker.bounds.width/2 - 35, height: 30))
        columnHourView.text = "h"
        columnHourView.textColor = UIColor.white
        myTimePicker.addSubview(columnHourView)
        let columnMinutesView = UILabel(frame: CGRect(x: myTimePicker.bounds.width-(myTimePicker.bounds.width/2)/3 , y: myTimePicker.bounds.height/2 - 15, width: myTimePicker.bounds.width/2 - 35, height: 30))
        columnMinutesView.text = "min"
        columnMinutesView.textColor = UIColor.white
        myTimePicker.addSubview(columnMinutesView)
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(dataPicker[component][row])
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return 24
        }
        return 60
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0
        {
            selectedHours = row
        }
        else {
            selectedMinutes = row
        }
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var attributedString:NSAttributedString!
        attributedString = NSAttributedString(string:String(dataPicker[component][row]),attributes:[NSForegroundColorAttributeName:UIColor.white])
        return attributedString
    }
    func counter() {
        seconds -= 1
        display.text = "\(String(format:"%02d",selectedHours)):\(String(format:"%02d",selectedMinutes)):\(seconds)"
        if seconds == 0{
            if selectedMinutes != 0 {
                selectedMinutes = selectedMinutes - 1
                seconds = 60
            }
            else if selectedMinutes == 0{
                if selectedHours != 0
                {
                    selectedHours = selectedHours - 1
                    selectedMinutes = 59
                    seconds = 60
                }
                else {
                    stopTimer()
                }
            }
            
        }
    }
    
    func stopTimer() {
        display.isHidden = true
        timer.invalidate()
        seconds = 60
        myTimePicker.isHidden = false
    }
    
    func createAllert(message:String){
        let allert = UIAlertController(title: "Attention", message: message, preferredStyle: UIAlertControllerStyle.alert)
        allert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            allert.dismiss(animated: true, completion: nil)
        }))
        self.present(allert, animated: true, completion: nil)
    }
    
    @IBAction func startTimer(_ sender: UIButton) {
        if selectedHours == 0 && selectedMinutes == 0{
            createAllert(message: "Enter time")
        }
        else {
            myTimePicker.isHidden = true
            display.isHidden = false 
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TimerViewController.counter), userInfo: nil, repeats: true)
        }
        
    }
    @IBAction func stopTimer(_ sender: UIButton) {
        stopTimer()
        display.text = " "
    }
}
