 

import UIKit

class AlarmClockTableViewController: UITableViewController,DataEnteredDelegate {
    var timer = Timer()
    var customCell = CustomAlarmClockTableViewCell()
    var selectedHours = [Int]()
    var seectedMinutes = [Int]()
    var repeating = [Bool]()
    var repeatingSound = [Bool]()
    var myDescription = [String]()
    var currentDate:String = " "
    var editHour = 0
    var editMin = 0
    var editRepeat = false
    var editRepeatSound = false
    var editDescription = " "
    func sendInfo(hours: Int, minutes: Int, repeating: Bool, repeatingSound: Bool, mydescription: String) {
        self.selectedHours.append(hours)
        self.seectedMinutes.append(minutes)
        self.repeating.append(repeating)
        self.repeatingSound.append(repeatingSound)
        self.myDescription.append(mydescription)
    }
    func setTime (){
        let date = NSDate()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "hh:mm"
        currentDate = dateFormat.string(from: date as Date)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAdd"
        {
            let secondVC = segue.destination as! AddAlarmClockViewController
            secondVC.delegate = self
            let scr = segue.destination as! AddAlarmClockViewController
            scr.defaultHour = editHour
            scr.defautMinute = editMin
            scr.defaultReapeat = editRepeat
            scr.defaultRepeatSigna = editRepeatSound
            scr.defaultDescription = editDescription

        }
        if segue.identifier == "toAddFromView"
        {
            let scr = segue.destination as! AddAlarmClockViewController
            scr.defaultHour = editHour
            scr.defautMinute = editMin
            scr.defaultReapeat = editRepeat
            scr.defaultRepeatSigna = editRepeatSound
            scr.defaultDescription = editDescription
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    func update() {
        if isEditing == false{
            self.tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isEditing = false
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        self.tableView.backgroundColor = UIColor.black
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.orange
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myDescription.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomAlarmClockTableViewCell
        cell.timeLabel.text = "\(String(format:"%02d",selectedHours[indexPath.row])):\(String(format:"%02d",seectedMinutes[indexPath.row]))"
        cell.timeLabel.textColor = UIColor.white
        cell.descriptionLabel.text = myDescription[indexPath.row]
        cell.descriptionLabel.textColor = UIColor.white
        cell.backgroundColor = UIColor.black
        setTime()
        if cell.isWorking.isOn == true{
            if cell.timeLabel.text == currentDate{
                cell.isWorking.setOn(false, animated: true)
            }
            else
            {
                cell.isWorking.setOn(true, animated: true)
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.selectedHours.remove(at: indexPath.row)
            self.seectedMinutes.remove(at: indexPath.row)
            self.repeating.remove(at: indexPath.row)
            self.repeatingSound.remove(at: indexPath.row)
            self.myDescription.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            
        }
    }
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.selectedHours[sourceIndexPath.row]
        selectedHours.remove(at: sourceIndexPath.row)
        selectedHours.insert(movedObject,at:destinationIndexPath.row)
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isEditing == true {
            editHour = selectedHours[indexPath.row]
            editMin = seectedMinutes[indexPath.row]
            editRepeat = repeating[indexPath.row]
            editRepeatSound = repeatingSound[indexPath.row]
            editDescription = myDescription[indexPath.row]
            selectedHours.remove(at:indexPath.row)
            seectedMinutes.remove(at:indexPath.row)
            repeating.remove(at:indexPath.row)
            repeatingSound.remove(at:indexPath.row)
            myDescription.remove(at:indexPath.row)
            tableView.reloadData()
            
            performSegue(withIdentifier: "toAdd", sender: self.view)
            
        }
        
    }
    
}
