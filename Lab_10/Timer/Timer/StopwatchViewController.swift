 

import UIKit
import QuartzCore

class StopwatchViewController: UIViewController {
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var display: UILabel!
    let model = HistoryViewController()
    var timeIntervals = [String]()
    private var timer = Timer()
    var startTime = TimeInterval()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func updateTime() {
        let currentTime = NSDate.timeIntervalSinceReferenceDate
        var elapsedTime = currentTime - startTime
        let minutes = Int(elapsedTime/60)
        elapsedTime -= (TimeInterval(minutes) * 60)
        let seconds = Int(elapsedTime)
        elapsedTime -= (TimeInterval(seconds))
        let miliseconds = Int(elapsedTime * 100)
        let correctMinutes = String(format: "%02d",minutes)
        let correctSeconds = String(format: "%02d",seconds)
        let correctMiliseconds = String(format: "%02d",miliseconds)
        display.text = "\(correctMinutes):\(correctSeconds):\(correctMiliseconds)"
        
    }
    
    @IBAction func startStopwatch(_ sender: UIButton) {
        if sender.currentTitle == "Start"{
            sender.setTitle("Stop", for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(StopwatchViewController.updateTime), userInfo: nil, repeats: true)
            startTime = NSDate.timeIntervalSinceReferenceDate
        }
        else {
            timeIntervals.append(display.text!)
            sender.setTitle("Start", for: .normal)
            timer.invalidate()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier) == "toHistory"
        {
            let scr = segue.destination as! HistoryViewController
            scr.colectionOfTimeIntervals = timeIntervals
        }
    }
    
}
