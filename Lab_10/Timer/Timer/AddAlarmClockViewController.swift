
import UIKit
protocol DataEnteredDelegate {
    func sendInfo(hours:Int, minutes:Int, repeating:Bool,repeatingSound:Bool,mydescription:String)
}

class AddAlarmClockViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate  {
    private var timer = Timer()
    var selectedHours = 0
    var selectedMinutes = 0
    var isRepeating = false
    var isReapeatingSignal = false
    var hoursData = [Int]()
    var minutesData = [Int]()
    var dataPicker = [[Int]]()
    var delegate:DataEnteredDelegate? = nil
    var defaultHour = 0
    var defautMinute = 0
    var defaultReapeat = false
    var defaultRepeatSigna = false
    var defaultDescription = " "
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var repeatSoundSwitch: UISwitch!
    @IBOutlet weak var repeatSwitch: UISwitch!
    @IBAction func repeatChooser(_ sender: UISwitch) {
        if (sender.isOn == true)
        {
            isRepeating = true
        }
        else {
            isRepeating = false
        }
    }
    @IBAction func isReapitingSignal(_ sender: UISwitch) {
        if (sender.isOn == true)
        {
            isReapeatingSignal = true
        }
        else {
            isReapeatingSignal = false
        }
    }
    
    @IBAction func saveData(_ sender: UIBarButtonItem) {
        if selectedHours == 0 && selectedMinutes == 0{
            createAllert(message: "Enter time")
        }
        else {
            delegate?.sendInfo(hours: selectedHours, minutes: selectedMinutes, repeating: isRepeating, repeatingSound: isReapeatingSignal, mydescription: descriptionField.text!)
        _ = self.navigationController?.popViewController(animated: true)
        }
    }
    @IBOutlet weak var timePicker: UIPickerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        hoursData.append(contentsOf: 0...23)
        minutesData.append(contentsOf: 0...59)
        dataPicker = [hoursData,minutesData]
        timePicker.backgroundColor = UIColor.black
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.orange
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.orange
    }
    override func viewWillAppear(_ animated: Bool) {
        timePicker.selectRow(defaultHour, inComponent: 0, animated: false)
        timePicker.selectRow(defaultHour, inComponent: 1, animated: false)
        repeatSwitch.setOn(defaultReapeat, animated: false)
        repeatSoundSwitch.setOn(defaultRepeatSigna, animated: false)
        descriptionField.text = defaultDescription
    }
       func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(dataPicker[component][row])
    }  
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return 24
        }
        return 60
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0
        {
            selectedHours = row
        }
        else {
            selectedMinutes = row
        }
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var attributedPickerString:NSAttributedString!
        attributedPickerString = NSAttributedString(string:String(dataPicker[component][row]),attributes:[NSForegroundColorAttributeName:UIColor.white])
        return attributedPickerString
    }
    func createAllert(message:String){
        let allert = UIAlertController(title: "Attention", message: message, preferredStyle: UIAlertControllerStyle.alert)
        allert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            allert.dismiss(animated: true, completion: nil)
        }))
        self.present(allert, animated: true, completion: nil)
    }


    }
